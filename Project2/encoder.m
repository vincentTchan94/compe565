function [quantYOut, quantCbOut, quantCrOut] = encoder(image, lTable, cTable)

dct = @(block_struct) dct2(block_struct.data);

Y = image(:,:,1);
Cb = image(:,:,2);
Cr = image(:,:,3);

% Downsample to 4:2:0
Ysample = Y;
Cbsample = Cb(1:2:end,1:2:end);
Crsample = Cr(1:2:end,1:2:end);

% Calculate dct of image
coeffY = blockproc(Ysample, [8 8], dct);
coeffCb = blockproc(Cbsample, [8 8], dct);
coeffCr = blockproc(Crsample, [8 8], dct);

% To display: 5th row, blocks 1 and 2, 
lumiImageBlock1 = coeffY((8*5)-7:(8*5),1:(1*8));
lumiImageBlock2 = coeffY((8*5)-7:(8*5),((1*8)+1:2*8));

disp('Block Row 5, Block 1:');
disp (coeffY((8*5)-7:(8*5),1:(1*8)));

disp('Block Row 5, Block 2:');
disp (coeffY((8*5)-7:(8*5),((1*8)+1:2*8)));

% Apply quantizer
quantY = @(block_struct) round(block_struct.data./lTable);
quantC = @(block_struct) round(block_struct.data./cTable);
quantYOut = blockproc(coeffY, [8 8], quantY);
quantCbOut = blockproc(coeffCb, [8 8], quantC);
quantCrOut = blockproc(coeffCr, [8 8], quantC);

fprintf('Block Row 5, Block 1 DC DCT Coefficient (After quantization): %.4f \n ', quantYOut((8*5)-7, 1));
fprintf('Block Row 5, Block 2 DC DCT Coefficient (After quantization): %.4f \n ', quantYOut((8*5)-7, 1));
figure('Name', 'Two blocks from first row');
subplot(2,1,1), imshow(lumiImageBlock1), title('Block One');
subplot(2,1,2), imshow(lumiImageBlock2), title('Block Two');
end
