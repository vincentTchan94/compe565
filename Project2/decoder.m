function decodedImage = decoder(Y, Cb, Cr, LumiT, ChromiT)

% Function Headers
dequantY = @(block_struct) round(block_struct.data.*LumiT);
dequantC = @(block_struct) round(block_struct.data.*ChromiT);
inverseDCT = @(block_struct) idct2(block_struct.data);

% Apply dequantizer to get DCT coeff
coeffY = blockproc(Y, [8 8], dequantY); 
coeffCb = blockproc(Cb, [8 8], dequantC);
coeffCr = blockproc(Cr, [8 8], dequantC);

% Apply inverse DCT to coeff
Ysample = blockproc(coeffY, [8 8], inverseDCT);
Cbsample = blockproc(coeffCb, [8 8], inverseDCT);
Crsample = blockproc(coeffCr, [8 8], inverseDCT);

% Upsample using Simple Row Replication
Cb_replic = repelem(Cbsample,2,2);
Cr_replic = repelem(Crsample,2,2);

% Return RGB image
decodedImage = uint8((cat(3, Ysample, Cb_replic, Cr_replic)));
decodedImage = ycbcr2rgb(decodedImage);

end