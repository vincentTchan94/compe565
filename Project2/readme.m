%CompE565 Homework 2
% Oct 23, 2017
% Name: Vincent Chan & Brandon Castro
% REDID: 815909699   |  816549663
%%%%%%%%%%%%

clear;

% INIT rgbImage
rgbImage = imread('image.jpg', 'jpg');
ycbcr = rgb2ycbcr(rgbImage);
LumiTable = xlsread('LuminanceTable.xlsx');
ChromiTable = xlsread('ChrominanceTable.xlsx');

[aYY, ayyCb, ayyCr] = encoder(ycbcr, LumiTable, ChromiTable);
decodedImage = decoder(aYY, ayyCb, ayyCr, LumiTable, ChromiTable);

figure('Name', 'Original vs Decoded');
subplot(1,2,1)
imshow(rgbImage)
subplot(1,2,2)
imshow(decodedImage)

% Display the error (original - reconstructed)
Yorig = rgbImage(:,:,1);
Ydec = decodedImage(:,:,1);
figure('Name', 'Error Image for Luminance');
imshow(Yorig - Ydec);

% Compute MSE
[M, N] = size(rgbImage);
error = rgbImage - decodedImage;
MSE = sum(sum(sum(error .^ 2)) * (1/(M * N)));
display(MSE);

% Compute PSNR
PSNR = 10*log10((255^2)/MSE);
display(PSNR)