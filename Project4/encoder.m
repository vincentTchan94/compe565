function [quantYOut, quantCbOut, quantCrOut] = encoder(image)

global Q_VALUE;
dct = @(block_struct) dct2(block_struct.data);

Y = image(:,:,1);
Cb = image(:,:,2);
Cr = image(:,:,3);

% Downsample to 4:2:0
Ysample = Y;
Cbsample = Cb(1:2:end,1:2:end);
Crsample = Cr(1:2:end,1:2:end);

% Calculate dct of image
coeffY = blockproc(Ysample, [8 8], dct);
coeffCb = blockproc(Cbsample, [8 8], dct);
coeffCr = blockproc(Crsample, [8 8], dct);

% Apply quantizer
quantY = @(block_struct) round(block_struct.data./Q_VALUE);
quantC = @(block_struct) round(block_struct.data./Q_VALUE);
quantYOut = blockproc(coeffY, [8 8], quantY);
quantCbOut = blockproc(coeffCb, [8 8], quantC);
quantCrOut = blockproc(coeffCr, [8 8], quantC);

end
