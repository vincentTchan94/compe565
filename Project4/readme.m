%CompE565 Homework 3
% Nov 11, 2017
% Name: Vincent Chan & Brandon Castro
% REDID: 815909699   |  816549663
%%%%%%%%%%%%

clear;

frames = read(VideoReader('football_qcif.avi'));
frames = frames(:,:,:,6:10); % 5 frames, 6 to 10
orig_frames = frames;

% Convert to YCbCr
orig_ycbcr_frames = uint8(zeros(size(frames)));
for i = 1:5
    frames(:,:,:,i)= rgb2ycbcr(frames(:,:,:,i));
    orig_ycbcr_frames(:,:,:,i)= rgb2ycbcr(frames(:,:,:,i));
end

global MB_SIZE P_SIZE Q_VALUE;
MB_SIZE = 16;
P_SIZE = 8;
Q_VALUE = 28;
MAX_ROWS = size(frames, 1);
MAX_COLS = size(frames, 2);
MAX_FRAMES = size(frames, 4);
CPU_TIME = zeros(5, 1);
CPU_TIME_ENCODE = zeros(5, 1);
BLOCKS_PER_ROW = MAX_ROWS/MB_SIZE;
BLOCKS_PER_COL = MAX_COLS/MB_SIZE;
residual = int16(zeros(MAX_ROWS,MAX_COLS,MAX_FRAMES));

% Motion Vectors (Dim: L x W x current[1] or predicted[2] x 5 frames) 
vector_x = zeros(BLOCKS_PER_ROW,BLOCKS_PER_COL,2,MAX_FRAMES);
vector_y = zeros(BLOCKS_PER_ROW,BLOCKS_PER_COL,2,MAX_FRAMES);

% Quantize and DCT frames
for i = 1:5
    CPU_TIME_ENCODE(i) = cputime;
    [Y, Cb, Cr] = encoder(frames(:,:,:,i));
    
    frames(:,:,:,i) = decoder(Y, Cb, Cr);
    CPU_TIME_ENCODE(i) = cputime-CPU_TIME_ENCODE(i);
end

% Frame loop, start motion predicting at 2 since frame 1 is i-frame
for n = 2:MAX_FRAMES
    CPU_TIME(n) = cputime;
    currentframe = frames(:,:,1, n);
    refframe = frames(:,:,1, n-1);
    
    vectorRow = 1;
    vectorCol = 1;

    % Macroblock loop
    % Steps of MB_SIZE
    for mb_row = 1:MB_SIZE:MAX_ROWS
        for mb_col = 1:MB_SIZE:MAX_COLS

        % Store position in vector
        vector_x(vectorRow, vectorCol, 1, n) = mb_col;
        vector_y(vectorRow, vectorCol, 1, n) = mb_row;

        % Bring in current MB
        currentMB = currentframe(mb_row:mb_row+MB_SIZE-1, mb_col:mb_col+MB_SIZE-1);

        % Check edges
        upper = mb_row - P_SIZE;
        if(upper < 1) upper = 1; end
        left = mb_col - P_SIZE;
        if(left < 1) left = 1; end
        lower = mb_row + MB_SIZE + P_SIZE;
        if(lower > MAX_ROWS) lower = MAX_ROWS; end
        right = mb_col + MB_SIZE + P_SIZE;
        if(right > MAX_COLS) right = MAX_COLS; end

        % Bring in reference MB, respective to top left
        refMB = refframe(upper:upper+MB_SIZE-1, left:left+MB_SIZE-1);
        
        % Setting starting value for min_MAD and match x, y coord
        min_MAD = sum(abs(int16(currentMB(:)) - int16(refMB(:))));
        match_row = upper;
        match_col = left;
        
        % Sets search window, starts Exhaustive Search
        for x = upper:lower-MB_SIZE+1
            for y = left:right-MB_SIZE+1
                refMB = refframe(x:x+MB_SIZE-1, y:y+MB_SIZE-1);
                MAD = mean(abs(int16(currentMB(:)) - int16(refMB(:))));
                if(MAD < min_MAD)
                    min_MAD = MAD;
                    match_row = x;
                    match_col = y;
                end
            end
        end
        
        % Save best match's x, y coordinates
        vector_x(vectorRow, vectorCol, 2, n) = match_col;
        vector_y(vectorRow, vectorCol, 2, n) = match_row;
        
        % Create residual
        best_match = refframe(match_row:match_row+MB_SIZE-1, match_col:match_col+MB_SIZE-1);
        residual(mb_row:mb_row+MB_SIZE-1,mb_col:mb_col+MB_SIZE-1,n) = int16(currentMB) - int16(best_match);

        vectorCol = vectorCol+1;
        end
        vectorRow = vectorRow+1;
        vectorCol = 1;
    end % End macroblock loop
    
    CPU_TIME(n) = cputime - CPU_TIME(n);
    fprintf('CPU_TIME for Frame %d: %.3f sec\n', n, CPU_TIME(n));
    
    % Motion Vector Representation 
    x_start = vector_x(:,:,1,n);
    x_end = vector_x(:,:,2,n) - x_start;
    y_start = vector_y(:,:,1,n);
    y_end = vector_y(:,:,2,n) - y_start;
    figure();
    quiver(x_start,y_start,x_end,y_end);
    title(['Motion vector for Frame ',num2str(n+MAX_FRAMES-1),' and Frame ',num2str(n+MAX_FRAMES)]);
end
fprintf('CPU_TIME_ENCODE Sum: %.3f sec\n', sum(CPU_TIME_ENCODE));
fprintf('CPU_TIME Average: %.3f sec\n', mean(CPU_TIME));

% Reconstructing frames from residual images
reconst_vid = int16(zeros(MAX_ROWS,MAX_COLS,MAX_FRAMES));
reconst_vid(:,:,1) = frames(:,:,1,1); % add i-frame back in
for n = 2:MAX_FRAMES
    row_count = 1;
    col_count = 1;
    for block_row = 1:MB_SIZE:MAX_ROWS
        for block_col = 1:MB_SIZE:MAX_COLS
            
            % Reconstruct
            row_vector = vector_y(row_count, col_count, 2, n);
            col_vector = vector_x(row_count, col_count, 2, n);
            reconst_vid(block_row:block_row+MB_SIZE-1, block_col:block_col+MB_SIZE-1, n) = ...
                reconst_vid(row_vector:row_vector+MB_SIZE-1, col_vector:col_vector+MB_SIZE-1, n-1) + ...
                residual(block_row:block_row+MB_SIZE-1, block_col:block_col+MB_SIZE-1, n);
            
            col_count = col_count + 1;
        end
        row_count = row_count + 1;
        col_count = 1;
    end
    
end

% Showing Frame Comparisons
decoded_frames = frames;
for n=1:MAX_FRAMES
    decoded_frames(:,:,1,n) = reconst_vid(:,:,n);
    
    figure();
    h(1) = subplot(2,2,1);
    imshow(orig_frames(:,:,:,n) );
    title(['Original Image for Frame ', num2str(n+MAX_FRAMES)])
    
    h(2) = subplot(2,2,2);
    imshow( ycbcr2rgb(decoded_frames(:,:,:,n)) );
    title(['Decoded Image for Frame ', num2str(n+MAX_FRAMES)])
    
     %I = orig_ycbcr_frames(:,:,:,n);
     %J = decoded_frames(:,:,:,n);    
     h(3) = subplot(2,2,3);
     pos = get(h,'Position');
     new = mean(cellfun(@(v)v(1),pos(1:2)));
     set(h(3),'Position',[new,pos{end}(2:end)])
     %imshowpair(I,J,'diff')
     %title("Error Image for Frame "+n)
    imshow(uint8(abs(residual(:,:,n))));
    title('Residual Error from Motion Estimation');
end