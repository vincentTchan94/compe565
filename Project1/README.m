%CompE565 Homework 1
% Sept 18, 2017
% Name: Vincent Chan & Brandon Castro
% REDID: 815909699   |  816549663
%%%%%%%%%%%%

% INIT rgbImage
rgbImage = imread('image.jpg', 'jpg');

%Problem 1: Display Original Image
disp('Problem 1: Display Original Image');
figure('Name', 'Original Image');
imshow(rgbImage), title('Original RGB Image');

% Problem 2: Display RGB colorbands 
disp('Problem 2: Display RGB Components');
Problem2(rgbImage);

%Problem 3: Convert image to YCbCr Color Space
disp('Problem 3: Convert Image to YCbCr');
ycbrImage = Problem3(rgbImage);

%Problem 4: Display Y, Cb, Cr branches seperately
disp('Problem 4: Display each YCbCr band');
Problem4(ycbrImage);

%Problem 5: Subsample Cb and Cr bands using 4:2:0 and display both bands
disp('Problem 5: Subsample Cb and Cr bands');
Problem5(ycbrImage);

%Problem 6: Upsample Cb and Cr bands using 2 methods
[interpImage, rowDelImage] = Problem6(ycbrImage);

%Problem 7: Convert the image into RGB format
disp('Problem 7: Converting YCbCr back to RGB image');
rgb_convertInt = ycbcr2rgb(ycbrImage);
rgb_convertRow = ycbcr2rgb(rowDelImage);

%Problem 8: Display the original and reconstructed images
disp('Problem 8: Displaying Converted Image vs Original Image');
figure('Name', 'Converted YCbCr vs Original');
subplot(3,1,1), imshow(rgb_convertInt), title("Interpolated Converted to RGB");
subplot(3,1,3), imshow(rgb_convertRow), title("Row Replication Converted to RGB");
subplot(3,1,2), imshow(rgbImage), title("Original RGB Image");

%Problem 9: Comment on the visual quality of both the upsampling cases
% On linear interpolation, can see visible yellow pixels in the waterfall
% On Row replicated, can see blurrish quality

%Problem 10: Measure MSE between the original and reconstructed 
%images (obtained using linear interpolation only)
format shortG
fprintf('The MSE of the RGB image and the YCbCr is %e', Problem10(rgbImage, rgb_convertInt));

%Problem 11: Comment on the compression ratio achieved by subsampling Cb 
%and Cr components for 4:2:0 approach. Please note that you do not send 
%the pixels which are made zero in the row and columns during subsampling.
