function Problem4(image)
    figure('Name', 'YCbCr Components');
    subplot(3,1,1), imshow(image(:,:,1)), title('Luminance (Y)'); %Luminance (Y)
    subplot(3,1,2), imshow(image(:,:,2)), title('Cb Component'); %Cb Component
    subplot(3,1,3), imshow(image(:,:,3)), title('Cr Component'); %Cr Component
end