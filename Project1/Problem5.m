function Problem5(image)
    [row, column, band] = size(image);
    
    figure('Name', 'Subsampled Cb Cr');
    for i = 1:2:column
        
        %Deleting the 2nd row
        for j = 2:2:row
            image(j, column, 2) = 0;
            image(j, column, 3) = 0;
        end
        
        %Deleting the 2nd columns
        for k = 1:row
            image(k, column+1, 2) = 0;
            image(k, column+1, 3) = 0;
        end
    end
    
    subplot(2,1,1), imshow(image(:,:,2)), title('Subsampled Cb Component');
    subplot(2,1,2), imshow(image(:,:,3)), title('Subsampled Cr Component');
end
