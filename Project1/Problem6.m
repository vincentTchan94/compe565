function [image_interp, image_replic] = Problem6(image)
    %% Problem 6.1: Upsampled Cb Cr using Linear Interpolation
    disp('Problem 6.1: Upsampled Cb Cr using Linear Interpolation');
    Y = image(:,:,1);
    Cb = image(:,:,2);
    Cr = image(:,:,3);
    
    % Using n = (n-1 + n-2) / 2 for each pixel in component
    Cb_interp = Cb;
    Cb_diff = Cb(1:2:end, 1:2:end) + [Cb(1:2:end, 3:2:end) Cb(1:2:end,end)];
    Cb_interp(1:2:end, 2:2:end) = (Cb_diff)/2;
    
    Cr_interp = Cr;
    Cr_diff = Cr(1:2:end, 1:2:end) + [Cr(1:2:end, 3:2:end) Cr(1:2:end,end)];
    Cr_interp(1:2:end, 2:2:end) = (Cb_diff)/2;
    
    % Display new image 
    image_interp = cat(3, Y, Cb_interp, Cr_interp);
    figure('Name', 'Upsampled using Linear Interpolation');
    imshow(ycbcr2rgb(image_interp)), title('Upsampled Image (Linear Interpolation)');
    
    %% Problem 6.2: Upsampled Cb Cr using Simple Row or Column Replication
    disp('Problem 6.2: Upsampled Cb Cr using Simple Row Replication');
    Cb_replic = Cb;
    Cb_replic(2:2:end, 2:2:end) = Cb_replic(1:2:end, 1:2:end);
    
    Cr_replic = Cr;
    Cr_replic(2:2:end, 2:2:end) = Cr_replic(1:2:end, 1:2:end);
    image_replic = cat(3, Y, Cb_replic, Cr_replic);
    figure('Name', 'Upsampled using Simple Row Replication');
    imshow(ycbcr2rgb(image_replic)), title('Upsampled Image (Simple Row Replication)');
end