%CompE565 Homework 3
% Nov 11, 2017
% Name: Vincent Chan & Brandon Castro
% REDID: 815909699   |  816549663
%%%%%%%%%%%%

clear;

v = VideoReader('football_qcif.avi');
fRange = read(v, [6 10]);

yCbCrFrames =  {};
[height, width, band] = size(fRange(:,:,:,1));
for i = 1:5
    frame = fRange(:,:,:,i);
    yCbCrFrames(i) = {rgb2ycbcr(frame)};
end

%Motion estimation
predictedFrames = {};
startX = zeros(1,99,4);
startY = zeros(1,99,4);
endX = zeros(1,99,4);
endY = zeros(1,99,4);
for curFrame = 2:5
    pFrame = zeros(height, width);
    referenceFrame = cell2mat(yCbCrFrames(curFrame-1));
    searchFrame = cell2mat(yCbCrFrames(curFrame));
    for blockNum = 1:99
        [newFrame, startX(1,blockNum,curFrame-1), startY(1,blockNum,curFrame-1), endX(1,blockNum, curFrame-1), endY(1,blockNum, curFrame-1)] = motionEst(referenceFrame(:,:,1), searchFrame(:,:,1), blockNum, 16, height, width);
        pFrame = pFrame + newFrame;
    end
    predictedFrames(curFrame-1) = {pFrame};
end
endX = endX - startX;
endY = endY - startY;

figure
quiver(startX(:,:,1),startY(:,:,1), endX(:,:,1), endY(:,:,1));
title('MV1: Frame 6 to 7');

figure
quiver(startX(:,:,2),startY(:,:,2), endX(:,:,2), endY(:,:,2));
title('MV1: Frame 7 to 8');

figure
quiver(startX(:,:,3),startY(:,:,3), endX(:,:,3), endY(:,:,3));
title('MV1: Frame 8 to 9');

figure
quiver(startX(:,:,4),startY(:,:,4), endX(:,:,4), endY(:,:,4));
title('MV1: Frame 9 to 10');