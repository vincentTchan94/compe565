function [predict, xPixel, yPx, endX, endY] = motionEst(refer, searchFrame, blockNum, blockSize, height, width)
    xBlock = (mod(blockNum-1, width/blockSize)) + 1;
    yBlock = idivide(int8(blockNum-1),width/blockSize, 'floor') + 1;
    
    xPixel = 1 + ((xBlock-1) * blockSize);
    yPx = double(1 + ((yBlock-1) * blockSize));
    
    %set up boarders
    left = xPixel - 8;
    if(left < 1) left = 1; end
    right = xPixel + blockSize + 8;
    if(right > width) right = width; end
    up = yPx - 8;
    if (up < 1) up = 1; end
    down = yPx + blockSize + 8;
    if (down > height) down = height; end
    
    %set up initial search
    predict = zeros (height, width);
    predict(up:up+15, left:left+15) = refer(up:up+15, left:left+15);
    refMat = refer(up:up+15, left:left+15);
    serMat = searchFrame(yPx:yPx+15, xPixel:xPixel+15);
    lowestVal = sum(abs(refMat - serMat));
    endX = xPixel;
    endY = yPx;
    
    %search for best fit
    for x = left+1 : right-24
        for y = up+1 : down-24
            refMat = refer(y:y+15, x:x+15);
            val = sum(abs(refMat - serMat));
            if(val < lowestVal) 
                endX = x;
                endY = y;
                lowestVal = val;
                predict = zeros (height, width);
                predict(y:y+15, x:x+15) = refer(y:y+15, x:x+15);
            end
        end
    end
    
    
    
end
